"use strict";

let num1 = +prompt("Enter first number: ", '');
console.log(num1);
let num2 = +prompt("Enter second number: ", '');
console.log(num2);
let operat = prompt("Enter operator (+, -, *, /): ", '');
console.log(operat);

function addition(num1, num2) {
    console.log(num1 + num2);
}
function subtraction(num1, num2) {
    console.log(num1 - num2);
}
function multiplication(num1, num2) {
    console.log(num1 * num2);
}
function division(num1, num2) {
    console.log(num1 / num2);
}

switch(operat) {
    case '+':
        addition(num1, num2);
        break;

    case '-':
        subtraction(num1, num2);
        break;

    case '*':
        multiplication(num1, num2);
        break;

    case '/':
        division(num1, num2);
        break;
}